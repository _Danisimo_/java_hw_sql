import com.mysql.cj.jdbc.JdbcConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConection {

    //static final String URL = "jdbc:ucanaccess://C://Users//Admin//Desktop//JAVA_HW_SQL//src//main//resources//DB.accdb";

    private static final String URL = "jdbc:mysql://eu-cdbr-west-03.cleardb.net";
    private static final String USER = "b5b161999e5503";
    private static final String PASSWORD = "0f6a06fe";

    private static Connection connect;
    private static Statement statement;

    public static void dbConnect(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your mySQL JDBC Driver?");
            e.printStackTrace();
            return;
        }

        System.out.println("mySQL JDBC Driver Registered!");

        try {
            connect = DriverManager.getConnection(URL,USER,PASSWORD);
            System.out.print("OK");
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console" + e);
            e.printStackTrace();
            return;
        }
    }

    public static void dbDisconnect(){
        try {
            if (connect != null && !connect.isClosed()) {
                connect.close();
            }
        } catch (Exception e){
             e.printStackTrace();
        }
    }

    public static Statement getStatement() {
        return statement;
    }

    public static void setStatement(Statement statement) {
        JdbcConection.statement = statement;
    }





}
