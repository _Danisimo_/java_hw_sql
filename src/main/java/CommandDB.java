import com.mysql.cj.jdbc.JdbcConnection;
import net.ucanaccess.commands.ICommand;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandDB {

    public static ResultSet result;

    public static int countPerson(){
       String command = "select count(*) from person";
       int count = 0;

       try{
            result =  JdbcConection.getStatement().executeQuery(command);

            while (result.next()){
                count = result.getInt(1);
            }
       }
       catch (SQLException e){
           e.printStackTrace();
       }
       return count;
    }

    public static int avarageAgeOfPerson(){
        String command = "select avg(age) from person";
        int avarage = 0;

        try{
            result = JdbcConection.getStatement().executeQuery(command);

            while(result.next()){
                avarage = result.getInt(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return avarage;
    }

    public static List<String> sortAlphabeticallyPersonByLastName() {

        String coommand = "select distinct lastName from person order by lastName asc";
        List<String> arrayList = new ArrayList<>();

        try {

            result = JdbcConection.getStatement().executeQuery(coommand);

            while (result.next()) {
                arrayList.add(result.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static Map<String,String> ascendingListOfCitizensByLastNameWithCount(){
        String command = "select lastName , count(*) from person group by lastName";
        Map<String, String> map = new HashMap<>();

        try{
            result = JdbcConection.getStatement().executeQuery(command);

            while(result.next()){
                map.put(result.getString(1),result.getString(2));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return map;


    }

    public static List<String> listOfCitizensContainBInLastName() {

        String command = "select lastName  from person where lastName like '%б%_'";

        List<String> arrayList = new ArrayList<>();

        try {

            result = JdbcConection.getStatement().executeQuery(command);

            while (result.next()) {
                arrayList.add(result.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static ArrayList<String> PersonWithoutHome()  {
        String query = "select * from person where idStreet is null";
        ArrayList<String> arrayList = new ArrayList<>();

        try {

            result = JdbcConection.getStatement().executeQuery(query);

            while (result.next()) {

                String persons = "";

                persons = "\nid=" + result.getString(1) + ", firstName=" + result.getString(2)
                        + ", lastName=" + result.getString(3) + ", age=" + result.getString(4);

                arrayList.add(persons);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return arrayList;
    }

    public static List<String> personLivingOnSpecialStreet()  {
        String query = "select * from person join street on person.idStreet = street.id where upper(name) like '%проспект Правды%' and person.age < 18";
        List<String> arrayList = new ArrayList<>();

        try {

            result = JdbcConection.getStatement().executeQuery(query);

            while (result.next()) {

                String persons = "";

                persons = "\nid=" + result.getString(1) + ", firstName=" + result.getString(2)
                        + ", lastName=" + result.getString(3) + ", age=" + result.getString(4)
                        + ", idStreet=" + result.getString(5);

                arrayList.add(persons);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return arrayList;
    }


}
